// ==UserScript==
// @id             1
// @name           MilliSkeet
// @version        1.0
// @namespace      StackOverflow
// @author         Borgleader
// @description    Displays StackOverflow reputation in milliSkeets
// @include        http://stackoverflow.com/*
// @run-at         document-end
// @require        http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js
// ==/UserScript==
this.$ = this.jQuery = jQuery.noConflict(true);

function getUserRep(userId)
{
    var urlPrefix = "http://api.stackexchange.com/2.1/users/";
    var urlSuffix = "?order=desc&sort=reputation&site=stackoverflow";
    var jsonUrl = urlPrefix + userId + urlSuffix;
    
    return $.getJSON(jsonUrl);
}

getUserRep(22656).done(function(data) {
    var skeetRep = data["items"][0]["reputation"];
    
    $(".user-details").each(function() {
        var userUrl = $(this).find("a").attr("href");
        if(userUrl)
        {
            var userId = parseInt(userUrl.split("/")[2], 10);
            var domElem = $(this).find(".reputation-score");
            getUserRep(userId).done(function(data) {
                var userRep = data["items"][0]["reputation"];
                var mSkeets = ((data["items"][0]["reputation"] / (1.0 * skeetRep)) * 1000).toFixed(3);
                domElem.html(mSkeets + " mS");
            });
        }
    });
    
    $("#hlinks-user").each(function() {
        var userUrl = $(this).find(".profile-link").attr("href");
        var userId = parseInt(userUrl.split("/")[2], 10);
        var domElem = $(this).find(".reputation-score");
        getUserRep(userId).done(function(data) {
            var mSkeets = ((data["items"][0]["reputation"] / (1.0 * skeetRep)) * 1000).toFixed(3);
            domElem.html(mSkeets + " mS");
        });
    });
});
